import requests
import json
import csv
import os

""" Weather API key can be obtained at https://home.openweathermap.org/ by creating an account
    
    Free tier allows up to 60 requests/hour for the following apis:
    - Current Weather
    - 5 day/3 hour forecast
    - Weather maps 1.0
    - UV index
    - Weather alerts

"""


def flatten_forecast_list(item):
    """ Takes a forecast item and creates new flat dict with info """
    new_dict = {
        "Time": item.get("dt_txt"),
        "Cloudcover": item.get("clouds").get("all"),
        "Windspeed": item.get("wind").get("speed"),
        "WindDirection": item.get("wind").get("deg"),
        "TempC": item.get("main").get("temp") - 272.15,
        "HumidityPrct": item.get("main").get("humidity"),
        "Weather": item.get("weather").pop().get("description") if len(item.get("weather")) > 0 else None
    }
    return new_dict


# define some parameters for building appropriate url (using lat,lon instead of recommended city id)
lat, lon = 45.995434, -66.656075
key = "f10d3b9e4dbfd44e97d2099a05ee5da6"  # my personal key for testing

# build a couple urls for requests
base_url = "http://api.openweathermap.org/data/2.5/"
weather_url = base_url + "weather?lat=%0.6f&lon=%0.6f&appid=%s" % (lat, lon, key)
forecast_url = base_url + "forecast?lat=%0.6f&lon=%0.6f&appid=%s" % (lat, lon, key)

# Define path for csv
output_path = os.path.expanduser("~/Desktop/weather.csv")

# Example call for current weather
print("Requesting current weather:")
r_weather = requests.request("GET", url=weather_url)
if r_weather.status_code == 200:
    # convert JSON string to python dictionary
    weather_dict = json.loads(r_weather.text)

    # extract some data from dictionary and print
    current_conditions = weather_dict["weather"][0]["main"]
    current_temp_c = weather_dict["main"]["temp"] - 272.15
    print("Current weather: %s at %0.0f degrees C" % (current_conditions, current_temp_c))
else:
    print("ERROR: %d" % r_weather.status_code)
print("---------------------------------------------")


# example call for weather forecast
print("Requesting forecast for every 3 hours over next 5 days")
r_forecast = requests.request("GET", url=forecast_url)
if r_forecast.status_code == 200:
    # convert to dictionary
    forecast_dict = json.loads(r_forecast.text)

    # for each entry, print the forecast time and expected conditions
    for i in forecast_dict["list"]:
        print("Weather for %s: %s" % (i["dt_txt"], i["weather"][0]["main"]))

else:
    print("ERROR: %d" % r_forecast.status_code)
    exit(1)


# write forecast to csv
with open(output_path, "w+", newline='') as csvfile:
    sample_forecast_item = forecast_dict["list"][0]
    fieldnames = list(flatten_forecast_list(sample_forecast_item).keys())
    weather_writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    weather_writer.writeheader()
    for i in forecast_dict["list"]:
        weather_writer.writerow(flatten_forecast_list(i))


